This repository has been archived. It was created to provide a minimal working example for [a question on GitLab Forum](https://forum.gitlab.com/t/gitlab-ci-cd-component-fail-with-jobs-specs-config-should-implement-a-script-or-a-trigger-keyword/101739) that was resolved. A [merge request to the documentation](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147531) to improve the documentation was submitted.

# Build and Push Docker Container Image to GESIS Registry

This is a [GitLab CI/CD component](https://docs.gitlab.com/ee/ci/components/).

